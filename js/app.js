var sessionStorageKeys = Object.keys(sessionStorage);
var nextpage;
function redirect(){
/*    window.location = $('.nextButton').attr('href'); */
   window.location = $('.nextPage').attr('href');
}
function playVideo() {
if (document.getElementsByTagName('video')[0]) {
	var video = document.getElementsByTagName('video')[0];
video.load();
// $('.play-video').click(function(){
video.play();
//	});
video.addEventListener('ended', function(e) {
if ($(".nextPage")[0]){
		nextpage = setTimeout(redirect, 5000);
		}
		$('.navButtons').toggle()
		$('.video-overlay').toggle()
    }, false);
    $('.replay').click(function(){
	    video.play();
	    clearTimeout(nextpage);
	    $('.video-overlay').toggle()
	});
	}
}
$(function() {
setTimeout(playVideo, 1000);
})
 //Audio
 var audio = document.getElementsByTagName('audio')[0];
/*
 audio = (function(e){
 e.preventDefault();
 });
*/
 audio.onended = function(e) {
     $('.replay-audio').toggle();
 };
 $('.replay-audio ').click(function() {
     audio.play();
     $('.replay-audio').toggle();
 });
 //Score
 var currentScore = 0;
 $('.current-score').html(currentScore); 
var storage = window.sessionStorage;
    //Score Management
    function getScore(section)
    {
        if(storage.getItem(section))
        {
            var score = storage.getItem(section);
        }
        else{
            storage.setItem(section,0);
            var score = 0;
        }
        return score;
    }

    function setScore(section,score)
    {
        storage.setItem(section,score);
    }

    function calculateTotalScore()
    {
        var canyonScore = getScore('canyon');
        var rapidsScore = getScore('rapids');
        var totalScore  = parseInt(rapidsScore) + parseInt(canyonScore);
        return totalScore;
    }


 $('.final-score').html(calculateTotalScore()); 
 
 $('.canyon-score').html(getScore('canyon'));
 $('.rapids-score').html(getScore('rapids'));
 
 /*
function scoreFeedback(){
 if (calculateTotalScore() >= 14){
	$('.feedback').html("You’ve mastered the concept of the 4Cs! Continue practicing and you will be well on your way to being a valuable asset to any company in the future.");  
 }
 elseif (calculateTotalScore() <= 7) {
$('.feedback').html("You’ve done a pretty good job applying the 4Cs. Review the tools from the 4Cs toolbox or from your Personal Brand Portfolio provided during class to help you master these skills. Continue practicing and you will be on your way to finding success in the workplace. Keep up the good work!");  
 }	 
   else{
		$('.feedback').html("You can improve on your application of the 4Cs skills by reviewing the tools in the 4Cs toolbox or in your Personal Brand Portfolio provided during class. You should continue practicing these skills so that become second nature to you. Mastering the 4Cs will put you on the right path to finding success in the workplace.");   
	 }
 }
*/
/*    $('.feedback').html(scoreFeedback);  */
 //Drag and Drop
 (function($) {
     // Detect touch support
     $.support.touch = 'ontouchend' in document;
     // Ignore browsers without touch support
     if (!$.support.touch) {
         return;
     }
     var mouseProto = $.ui.mouse.prototype,
         _mouseInit = mouseProto._mouseInit,
         touchHandled;

     function simulateMouseEvent(event, simulatedType) { //use this function to simulate mouse event
         // Ignore multi-touch events
         if (event.originalEvent.touches.length > 1) {
             return;
         }
         event.preventDefault(); //use this to prevent scrolling during ui use
         var touch = event.originalEvent.changedTouches[0],
             simulatedEvent = document.createEvent('MouseEvents');
         // Initialize the simulated mouse event using the touch event's coordinates
         simulatedEvent.initMouseEvent(
         simulatedType, // type
         true, // bubbles                    
         true, // cancelable                 
         window, // view                       
         1, // detail                     
         touch.screenX, // screenX                    
         touch.screenY, // screenY                    
         touch.clientX, // clientX                    
         touch.clientY, // clientY                    
         false, // ctrlKey                    
         false, // altKey                     
         false, // shiftKey                   
         false, // metaKey                    
         0, // button                     
         null // relatedTarget              
         );
         // Dispatch the simulated event to the target element
         event.target.dispatchEvent(simulatedEvent);
     }
     mouseProto._touchStart = function(event) {
         var self = this;
         // Ignore the event if another widget is already being handled
         if (touchHandled || !self._mouseCapture(event.originalEvent.changedTouches[0])) {
             return;
         }
         // Set the flag to prevent other widgets from inheriting the touch event
         touchHandled = true;
         // Track movement to determine if interaction was a click
         self._touchMoved = false;
         // Simulate the mouseover event
         simulateMouseEvent(event, 'mouseover');
         // Simulate the mousemove event
         simulateMouseEvent(event, 'mousemove');
         // Simulate the mousedown event
         simulateMouseEvent(event, 'mousedown');
     };
     mouseProto._touchMove = function(event) {
         // Ignore event if not handled
         if (!touchHandled) {
             return;
         }
         // Interaction was not a click
         this._touchMoved = true;
         // Simulate the mousemove event
         simulateMouseEvent(event, 'mousemove');
     };
     mouseProto._touchEnd = function(event) {
         // Ignore event if not handled
         if (!touchHandled) {
             return;
         }
         // Simulate the mouseup event
         simulateMouseEvent(event, 'mouseup');
         // Simulate the mouseout event
         simulateMouseEvent(event, 'mouseout');
         // If the touch interaction did not move, it should trigger a click
         if (!this._touchMoved) {
             // Simulate the click event
             simulateMouseEvent(event, 'click');
         }
         // Unset the flag to allow other widgets to inherit the touch event
         touchHandled = false;
     };
     mouseProto._mouseInit = function() {
         var self = this;
         // Delegate the touch handlers to the widget's element
         self.element.on('touchstart', $.proxy(self, '_touchStart')).on('touchmove', $.proxy(self, '_touchMove')).on('touchend', $.proxy(self, '_touchEnd'));
         // Call the original $.ui.mouse init method
         _mouseInit.call(self);
     };
 })(jQuery);
 
     $(function() {
     $(".choice").draggable({
         revert: true
     });

     $(".answer-box").droppable({
         drop: function(event, ui) {

            var section  =  event.target.id;

             $('.answer-box').addClass('answered');
             $(".ui-draggable-dragging").removeClass('choose');
             $('.ui-draggable-dragging p').addClass('accepted');
             $('.ui-draggable-dragging .feedback').addClass('accepted');
             // get the feedback from the hidden span 
             /* $('.accepted').toggle();  */
             $('.feedback').toggle(); 
             $('.questions').toggle(); 
             $('.ui-draggable-dragging .feedback span').addClass('feedback-personal');
             $(this).find('h1').html(function() {
                 return $('.feedback-personal').text();
             });
             
             
             // get the points from the hidden span  
             $('.ui-draggable-dragging .points').addClass('worth');
             var tmpScore = $('.worth').text();
             $('.current-score').html(getScore(section));
             // cloning and appending prevents the revert animation from still occurring
             ui.draggable.clone(true).css('position', 'inherit').appendTo($(this));
             ui.draggable.remove();
             $('.choose').remove();
             $('.accepted').find('audio')[0].play();
             $(this).droppable('disable');
             
             console.log('section is: ' + event.target.id);
            switch (event.target.id)
            {
                case 'canyon':
                    console.log('canyon');
                    setScore('canyon',tmpScore);
                    $('.current-score').html(getScore('canyon'));
                    break;
                case 'rapids':
                    console.log('rapids');
                    setScore('rapids',tmpScore);
                    $('.current-score').html(getScore('rapids'));
                    break;
                    
            }
    
         }
     });
 });   
      
 //Captions
$(function() {
			
			$("html").addClass("js");
			$(".caption li").hide();
	
			var v = document.getElementsByTagName('video')[0];
			
			v.addEventListener("timeupdate", function() {
			
				$(".timer span").text(parseFloat(v.currentTime.toFixed(2)));
				
				var time = Math.floor(v.currentTime);
				
				if($(".caption li[title='"+time+"']").not(".show").length) {
					
					$(".caption li[title='"+time+"']").addClass("show").fadeIn();
					$(".caption li[title!='"+time+"']").removeClass("show").fadeOut();
				
				}
			
			}, true);
			
		 });
  // toggle button functionality
  // if element not visible, show it and set sessionStorage
  // hide element and clear sessionStorage otherwise
  $('.captionsBtn').click(function(){
      if($('.captionsBtn').hasClass('off')){
       sessionStorage.setItem('captionOn', true);
       $('.caption').css('display', 'inline');
	$('.captionsBtn').removeClass('off').addClass('on');
      } else{
      sessionStorage.setItem('captionOn', false);
	  $('.caption').css('display', 'none');
$('.captionsBtn').removeClass('on').addClass('off');
      }
  });


/*
var capttionToggle = 1;

 function changePic()
{
 if (capttionToggle == 1){

 capttionToggle = 0;
 }

 else if (capttionToggle == 0)
 {

 capttionToggle = 1;
 }
}
function changeIndexPic()
{
 if (capttionToggle == 1){
 document.getElementById("captionBtn").src = "images/CaptionsBtn.png";
 capttionToggle = 0;
 }

 else if (capttionToggle == 0)
 {
 document.getElementById("captionBtn").src = "images/CaptionsBtnOff.png";
 capttionToggle = 1;
 }
}
*/