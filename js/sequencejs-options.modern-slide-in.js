$(document).ready(function(){
    var options = {
        nextButton: true,
        prevButton: true,
        showNextButtonOnInit:true,
        showPreviousButtonOnInit:true,
        animateStartingFrameIn: true,
        cycle:true,
        reverseAnimationsWhenNavigatingBackwards:true,
        autoPlay:false,
        swipeNavigation: true,
        swipeThreshold:10,
        swipeEvents: {left: "prev", right: "next", up: false, down: false},
        navigationSkip:false,
        preloader: true,
        pauseOnHover: false,
        preloadTheseFrames: [1]
        /*
         *preloadTheseImages: [
            "images/tn-model1.png",
            "images/tn-model2.png",
            "images/tn-model3.png"
        
        ]*/
    };
    
   
    
    /*var sequence1 = $("#sequence1").sequence(options).data("sequence");*/
    var sequence = $("#sequence").sequence(options).data("sequence");

     sequence.afterLoaded = function() {
        $("#sequence-theme .nav").fadeIn(100);
        $("#sequence-theme .nav li:nth-child("+(sequence.settings.startingFrameID)+") ").addClass("active");
    }
    
    sequence.beforeNextFrameAnimatesIn = function() {
        $("#sequence-theme .nav li:not(:nth-child("+(sequence.nextFrameID)+"))").removeClass("active");
        $("#sequence-theme .nav li:nth-child("+(sequence.nextFrameID)+")").addClass("active");
    }
   
    $("#sequence-theme").on("click", ".nav li", function() {
        $(this).removeClass("active").addClass("active");
        sequence.nextFrameID = $(this).index()+1;
        sequence.goTo(sequence.nextFrameID);
    });
});