/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='../images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'JAinterface',
            type:'image',
            rect:['0','0','2048px','1536px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"JAinterface.PNG",'0px','0px']
         },
         {
            id:'RagingRapidsMap',
            type:'image',
            rect:['0','0','2048px','1536px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"RagingRapidsMap.png",'0px','0px']
         },
         {
            id:'ragingrapids',
            type:'image',
            rect:['0','0','1608px','997px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"ragingrapids.png",'0px','0px']
         },
         {
            id:'ViewLookingUpWaterSlide3',
            type:'image',
            rect:['-882px','0','3000px','1536px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"ViewLookingUpWaterSlide.PNG",'0px','0px']
         },
         {
            id:'RagingRapidsLine',
            type:'image',
            rect:['0','0','2793px','1536px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"RagingRapidsLine.jpg",'0px','0px']
         },
         {
            id:'RRNervousManager',
            type:'image',
            rect:['422px','398px','432px','348px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"RRNervousManager.PNG",'0px','0px']
         },
         {
            id:'RRNervousLG3',
            type:'image',
            rect:['1089px','294px','325px','452px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"RRNervousLG.png",'0px','0px']
         },
         {
            id:'invisBtn',
            type:'rect',
            rect:['0','234','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'invisBtn',
            symbolName:'invisBtn'
         }
         ]
      },
   states: {
      "Base State": {
         "${_ragingrapids}": [
            ["style", "top", '-45px'],
            ["transform", "scaleY", '0.45'],
            ["style", "height", '575px'],
            ["transform", "scaleX", '0.45'],
            ["style", "opacity", '0'],
            ["style", "left", '-253px'],
            ["style", "width", '930px']
         ],
         "${_RagingRapidsMap}": [
            ["style", "height", '767px'],
            ["style", "opacity", '0'],
            ["style", "width", '1022px']
         ],
         "${_RagingRapidsLine}": [
            ["style", "top", '0px'],
            ["style", "height", '1590px'],
            ["style", "opacity", '0'],
            ["style", "left", '0px'],
            ["style", "width", '2385px']
         ],
         "${_RRNervousLG3}": [
            ["style", "left", '1048px'],
            ["style", "top", '294px']
         ],
         "${_RRNervousManager}": [
            ["style", "left", '1016px'],
            ["style", "top", '398px']
         ],
         "${_invisBtn}": [
            ["style", "top", '-5px'],
            ["transform", "scaleY", '0.5'],
            ["style", "display", 'block'],
            ["style", "left", '-226px'],
            ["transform", "scaleX", '0.47113']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(0,0,0,1.00)'],
            ["style", "width", '1022px'],
            ["style", "height", '746px'],
            ["style", "overflow", 'hidden']
         ],
         "${_JAinterface}": [
            ["style", "height", '767px'],
            ["style", "opacity", '1'],
            ["style", "width", '1022px']
         ],
         "${_ViewLookingUpWaterSlide3}": [
            ["style", "top", '0px'],
            ["style", "height", '746px'],
            ["style", "opacity", '0'],
            ["style", "left", '-114px'],
            ["style", "width", '1458px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 22000,
         autoPlay: true,
         timeline: [
            { id: "eid151", tween: [ "transform", "${_invisBtn}", "scaleY", '0.5', { fromValue: '0.5'}], position: 0, duration: 0 },
            { id: "eid97", tween: [ "style", "${_JAinterface}", "width", '1022px', { fromValue: '1022px'}], position: 0, duration: 0 },
            { id: "eid153", tween: [ "style", "${_invisBtn}", "top", '-5px', { fromValue: '-5px'}], position: 0, duration: 0 },
            { id: "eid126", tween: [ "style", "${_ViewLookingUpWaterSlide3}", "width", '1458px', { fromValue: '1458px'}], position: 7075, duration: 0 },
            { id: "eid158", tween: [ "style", "${_RRNervousManager}", "left", '421px', { fromValue: '1016px'}], position: 14750, duration: 2250 },
            { id: "eid161", tween: [ "style", "${_RRNervousLG3}", "left", '690px', { fromValue: '1048px'}], position: 16000, duration: 3000 },
            { id: "eid3", tween: [ "style", "${_RagingRapidsMap}", "opacity", '1', { fromValue: '0'}], position: 3000, duration: 2000 },
            { id: "eid24", tween: [ "style", "${_RagingRapidsMap}", "opacity", '0.25', { fromValue: '1'}], position: 5000, duration: 1000 },
            { id: "eid64", tween: [ "style", "${_RagingRapidsMap}", "opacity", '0.25', { fromValue: '0.25'}], position: 7075, duration: 0 },
            { id: "eid150", tween: [ "transform", "${_invisBtn}", "scaleX", '0.47113', { fromValue: '0.47113'}], position: 0, duration: 0 },
            { id: "eid139", tween: [ "style", "${_RagingRapidsLine}", "width", '2293px', { fromValue: '2385px'}], position: 8500, duration: 1250 },
            { id: "eid130", tween: [ "style", "${_RagingRapidsLine}", "width", '1129px', { fromValue: '1906px'}], position: 9750, duration: 5000 },
            { id: "eid10", tween: [ "transform", "${_ragingrapids}", "scaleY", '1', { fromValue: '0.45'}], position: 5000, duration: 3000 },
            { id: "eid142", tween: [ "style", "${_RagingRapidsLine}", "left", '-5px', { fromValue: '0px'}], position: 8500, duration: 6250 },
            { id: "eid134", tween: [ "style", "${_RagingRapidsLine}", "left", '0px', { fromValue: '0px'}], position: 9750, duration: 0 },
            { id: "eid79", tween: [ "style", "${_RagingRapidsLine}", "left", '-8px', { fromValue: '-5px'}], position: 14750, duration: 0 },
            { id: "eid141", tween: [ "style", "${_RagingRapidsLine}", "top", '0px', { fromValue: '0px'}], position: 8500, duration: 0 },
            { id: "eid155", tween: [ "style", "${_RagingRapidsLine}", "top", '-7px', { fromValue: '0px'}], position: 9750, duration: 5000 },
            { id: "eid156", tween: [ "style", "${_RRNervousManager}", "top", '398px', { fromValue: '398px'}], position: 14750, duration: 0 },
            { id: "eid138", tween: [ "style", "${_RagingRapidsLine}", "height", '1529px', { fromValue: '1590px'}], position: 8500, duration: 1250 },
            { id: "eid129", tween: [ "style", "${_RagingRapidsLine}", "height", '753px', { fromValue: '1271px'}], position: 9750, duration: 5000 },
            { id: "eid41", tween: [ "style", "${_ViewLookingUpWaterSlide3}", "opacity", '1', { fromValue: '0'}], position: 7075, duration: 681 },
            { id: "eid147", tween: [ "style", "${_ViewLookingUpWaterSlide3}", "opacity", '0', { fromValue: '1'}], position: 10147, duration: 43 },
            { id: "eid17", tween: [ "style", "${_JAinterface}", "opacity", '0', { fromValue: '1'}], position: 4940, duration: 60 },
            { id: "eid18", tween: [ "style", "${_JAinterface}", "opacity", '0', { fromValue: '0'}], position: 5000, duration: 0 },
            { id: "eid26", tween: [ "style", "${_invisBtn}", "display", 'none', { fromValue: 'block'}], position: 5500, duration: 0 },
            { id: "eid125", tween: [ "style", "${_ViewLookingUpWaterSlide3}", "height", '746px', { fromValue: '746px'}], position: 7075, duration: 0 },
            { id: "eid12", tween: [ "style", "${_ragingrapids}", "left", '-91px', { fromValue: '-253px'}], position: 5000, duration: 2075 },
            { id: "eid104", tween: [ "style", "${_ragingrapids}", "left", '9px', { fromValue: '-91px'}], position: 7075, duration: 925 },
            { id: "eid96", tween: [ "style", "${_JAinterface}", "height", '767px', { fromValue: '767px'}], position: 0, duration: 0 },
            { id: "eid98", tween: [ "style", "${_RagingRapidsMap}", "height", '767px', { fromValue: '767px'}], position: 0, duration: 0 },
            { id: "eid40", tween: [ "style", "${_ViewLookingUpWaterSlide3}", "left", '-234px', { fromValue: '-114px'}], position: 7075, duration: 3072 },
            { id: "eid76", tween: [ "style", "${_RagingRapidsLine}", "opacity", '0', { fromValue: '0'}], position: 0, duration: 0 },
            { id: "eid75", tween: [ "style", "${_RagingRapidsLine}", "opacity", '1', { fromValue: '0'}], position: 9000, duration: 2250 },
            { id: "eid152", tween: [ "style", "${_invisBtn}", "left", '-226px', { fromValue: '-226px'}], position: 0, duration: 0 },
            { id: "eid6", tween: [ "style", "${_ragingrapids}", "opacity", '1', { fromValue: '0'}], position: 5000, duration: 2075 },
            { id: "eid8", tween: [ "transform", "${_ragingrapids}", "scaleX", '1', { fromValue: '0.45'}], position: 5000, duration: 3000 },
            { id: "eid111", tween: [ "style", "${_ragingrapids}", "width", '1192px', { fromValue: '930px'}], position: 5000, duration: 2075 },
            { id: "eid113", tween: [ "style", "${_ragingrapids}", "width", '1004px', { fromValue: '1192px'}], position: 7075, duration: 925 },
            { id: "eid124", tween: [ "style", "${_ViewLookingUpWaterSlide3}", "top", '0px', { fromValue: '0px'}], position: 7075, duration: 0 },
            { id: "eid160", tween: [ "style", "${_RRNervousLG3}", "top", '294px', { fromValue: '294px'}], position: 16000, duration: 0 },
            { id: "eid14", tween: [ "style", "${_ragingrapids}", "top", '-8px', { fromValue: '-45px'}], position: 5000, duration: 2075 },
            { id: "eid105", tween: [ "style", "${_ragingrapids}", "top", '52px', { fromValue: '-8px'}], position: 7075, duration: 925 },
            { id: "eid99", tween: [ "style", "${_RagingRapidsMap}", "width", '1022px', { fromValue: '1022px'}], position: 0, duration: 0 },
            { id: "eid110", tween: [ "style", "${_ragingrapids}", "height", '738px', { fromValue: '575px'}], position: 5000, duration: 2075 },
            { id: "eid112", tween: [ "style", "${_ragingrapids}", "height", '622px', { fromValue: '738px'}], position: 7075, duration: 925 }         ]
      }
   }
},
"invisBtn": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','0px','866px','502px','auto','auto'],
      borderRadius: ['50%','50%','50%','50%'],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'Ellipse',
      opacity: 0,
      type: 'ellipse',
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_Ellipse}": [
            ["style", "top", '0px'],
            ["style", "opacity", '0'],
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '502px'],
            ["style", "width", '866px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-14460795");
