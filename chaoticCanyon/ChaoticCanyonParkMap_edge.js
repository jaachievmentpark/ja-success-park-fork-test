/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='../images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'JAinterface',
            type:'image',
            rect:['0','0','2048px','1536px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"JAinterface.PNG",'0px','0px']
         },
         {
            id:'ChaoticCanyonMap',
            type:'image',
            rect:['0','0','2048px','1536px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"ChaoticCanyonMap.png",'0px','0px']
         },
         {
            id:'ChaoticCanyonRollerCoaster',
            type:'image',
            rect:['0','0','2000px','2500px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"ChaoticCanyonRollerCoaster.PNG",'0px','0px']
         },
         {
            id:'RollerCoasterTrackBG',
            type:'image',
            rect:['0','0','1022px','746px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"RollerCoasterTrackBG.JPG",'0px','0px']
         },
         {
            id:'RollerCoasterKids',
            type:'image',
            rect:['-968px','740px','1022px','746px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"RollerCoasterKids.PNG",'0px','0px']
         },
         {
            id:'Rectangle2',
            type:'rect',
            rect:['-17px','-11px','1060px','777px','auto','auto'],
            opacity:1,
            fill:["rgba(0,0,0,1.00)"],
            stroke:[0,"rgb(0, 0, 0)","none"]
         },
         {
            id:'invisBtn',
            type:'rect',
            rect:['0','234','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'invisBtn',
            symbolName:'invisBtn'
         }
         ]
      },
   states: {
      "Base State": {
         "${_Rectangle2}": [
            ["color", "background-color", 'rgba(0,0,0,1.00)'],
            ["style", "opacity", '0']
         ],
         "${_ChaoticCanyonMap}": [
            ["style", "height", '766px'],
            ["style", "opacity", '0'],
            ["style", "width", '1022px']
         ],
         "${_ChaoticCanyonRollerCoaster}": [
            ["style", "top", '210px'],
            ["style", "height", '672px'],
            ["style", "opacity", '0.15'],
            ["style", "left", '631px'],
            ["style", "width", '537px']
         ],
         "${_RollerCoasterTrackBG}": [
            ["style", "opacity", '0']
         ],
         "${_invisBtn}": [
            ["style", "top", '167px'],
            ["transform", "scaleY", '0.74794'],
            ["style", "display", 'block'],
            ["style", "left", '475px'],
            ["transform", "scaleX", '0.26328']
         ],
         "${_RollerCoasterKids}": [
            ["style", "top", '546px'],
            ["style", "opacity", '0'],
            ["style", "left", '-626px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(0,0,0,1.00)'],
            ["style", "width", '1022px'],
            ["style", "height", '746px'],
            ["style", "overflow", 'hidden']
         ],
         "${_JAinterface}": [
            ["style", "height", '766px'],
            ["style", "opacity", '1'],
            ["style", "width", '1022px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 13000,
         autoPlay: true,
         timeline: [
            { id: "eid133", tween: [ "transform", "${_invisBtn}", "scaleY", '0.74794', { fromValue: '0.74794'}], position: 0, duration: 0 },
            { id: "eid210", tween: [ "style", "${_JAinterface}", "width", '1022px', { fromValue: '1022px'}], position: 0, duration: 0 },
            { id: "eid17", tween: [ "style", "${_JAinterface}", "opacity", '0', { fromValue: '1'}], position: 4940, duration: 60 },
            { id: "eid18", tween: [ "style", "${_JAinterface}", "opacity", '0', { fromValue: '0'}], position: 5000, duration: 0 },
            { id: "eid211", tween: [ "style", "${_invisBtn}", "top", '167px', { fromValue: '167px'}], position: 0, duration: 0 },
            { id: "eid186", tween: [ "style", "${_ChaoticCanyonRollerCoaster}", "opacity", '1', { fromValue: '0.15'}], position: 5008, duration: 1642 },
            { id: "eid225", tween: [ "style", "${_RollerCoasterTrackBG}", "opacity", '0', { fromValue: '0'}], position: 0, duration: 0 },
            { id: "eid227", tween: [ "style", "${_RollerCoasterTrackBG}", "opacity", '1', { fromValue: '0'}], position: 7500, duration: 558 },
            { id: "eid26", tween: [ "style", "${_invisBtn}", "display", 'none', { fromValue: 'block'}], position: 5250, duration: 0 },
            { id: "eid22", tween: [ "color", "${_Stage}", "background-color", 'rgba(0,0,0,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,0,0,1.00)'}], position: 0, duration: 0 },
            { id: "eid132", tween: [ "transform", "${_invisBtn}", "scaleX", '0.26328', { fromValue: '0.26328'}], position: 0, duration: 0 },
            { id: "eid235", tween: [ "style", "${_RollerCoasterKids}", "opacity", '1', { fromValue: '0'}], position: 8058, duration: 67 },
            { id: "eid208", tween: [ "style", "${_ChaoticCanyonMap}", "width", '1022px', { fromValue: '1022px'}], position: 0, duration: 0 },
            { id: "eid232", tween: [ "style", "${_RollerCoasterKids}", "left", '8px', { fromValue: '-626px'}], position: 8125, duration: 1192 },
            { id: "eid97", tween: [ "style", "${_ChaoticCanyonMap}", "opacity", '1', { fromValue: '0'}], position: 3000, duration: 2000 },
            { id: "eid95", tween: [ "style", "${_ChaoticCanyonMap}", "opacity", '0.25', { fromValue: '1'}], position: 5000, duration: 1000 },
            { id: "eid192", tween: [ "style", "${_ChaoticCanyonMap}", "opacity", '0.25', { fromValue: '0.25'}], position: 8340, duration: 0 },
            { id: "eid190", tween: [ "style", "${_ChaoticCanyonRollerCoaster}", "width", '886px', { fromValue: '537px'}], position: 5008, duration: 1642 },
            { id: "eid209", tween: [ "style", "${_JAinterface}", "height", '766px', { fromValue: '766px'}], position: 0, duration: 0 },
            { id: "eid233", tween: [ "style", "${_RollerCoasterKids}", "top", '98px', { fromValue: '546px'}], position: 8125, duration: 1192 },
            { id: "eid187", tween: [ "style", "${_ChaoticCanyonRollerCoaster}", "height", '1109px', { fromValue: '672px'}], position: 5008, duration: 1642 },
            { id: "eid212", tween: [ "style", "${_invisBtn}", "left", '475px', { fromValue: '475px'}], position: 0, duration: 0 },
            { id: "eid188", tween: [ "style", "${_ChaoticCanyonRollerCoaster}", "top", '-56px', { fromValue: '210px'}], position: 5008, duration: 1642 },
            { id: "eid239", tween: [ "style", "${_Rectangle2}", "opacity", '1', { fromValue: '0.000000'}], position: 12000, duration: 1000 },
            { id: "eid189", tween: [ "style", "${_ChaoticCanyonRollerCoaster}", "left", '183px', { fromValue: '631px'}], position: 5008, duration: 1642 },
            { id: "eid193", tween: [ "style", "${_ChaoticCanyonRollerCoaster}", "left", '68px', { fromValue: '183px'}], position: 6650, duration: 1408 },
            { id: "eid207", tween: [ "style", "${_ChaoticCanyonMap}", "height", '766px', { fromValue: '766px'}], position: 0, duration: 0 }         ]
      }
   }
},
"invisBtn": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','0px','866px','502px','auto','auto'],
      borderRadius: ['50%','50%','50%','50%'],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'Ellipse',
      opacity: 0,
      type: 'ellipse',
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_Ellipse}": [
            ["style", "top", '0px'],
            ["style", "opacity", '0'],
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '502px'],
            ["style", "width", '866px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-14460795");
